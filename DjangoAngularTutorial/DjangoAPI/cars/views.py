from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .models import Brand, ModelCar, Price, Year, Fuel, Car
from .serializers import BrandSerializer, ModelCarSerializer, PriceSerializer, YearSerializer, FuelSerializer, \
    CarSerializer
from django.http.response import JsonResponse
from django.core.files.storage import  default_storage

# Create your views here.
@csrf_exempt
def brandApi(request, id=0):
    if request.method == 'GET':
        brands = Brand.objects.all()
        brands_serializer = BrandSerializer(brands, many=True)
        return JsonResponse(brands_serializer.data, safe=False)
    elif request.method == 'POST':
        brand_data = JSONParser().parse(request)
        brand_serializer = BrandSerializer(data=brand_data)
        if brand_serializer.is_valid():
            brand_serializer.save()
            return JsonResponse("Added Successfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    elif request.method == 'PUT':
        brand_data = JSONParser().parse(request)
        brand = Brand.objects.get(brandId=brand_data['brandId'])
        brand_serializer = BrandSerializer(brand, data=brand_data)
        if brand_serializer.is_valid():
            brand_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method == 'DELETE':
        brand = Brand.objects.get(brandId=id)
        brand.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)


@csrf_exempt
def modelCarApi(request, id=0):
    if request.method == 'GET':
        modelCars = ModelCar.objects.all()
        modelCars_serializer = ModelCarSerializer(modelCars, many=True)
        return JsonResponse(modelCars_serializer.data, safe=False)
    elif request.method == 'POST':
        modelCar_data = JSONParser().parse(request)
        modelCar_serializer = ModelCarSerializer(data=modelCar_data)
        if modelCar_serializer.is_valid():
            modelCar_serializer.save()
            return JsonResponse("Added Successfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    elif request.method == 'PUT':
        modelCar_data = JSONParser().parse(request)
        modelCar = ModelCar.objects.get(modelId=modelCar_data['modelId'])
        modelCar_serializer = ModelCarSerializer(modelCar, data=modelCar_data)
        if modelCar_serializer.is_valid():
            modelCar_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method == 'DELETE':
        modelCar = ModelCar.objects.get(modelId=id)
        modelCar.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)


@csrf_exempt
def priceApi(request, id=0):
    if request.method == 'GET':
        prices = Price.objects.all()
        prices_serializer = PriceSerializer(prices, many=True)
        return JsonResponse(prices_serializer.data, safe=False)
    elif request.method == 'POST':
        price_data = JSONParser().parse(request)
        price_serializer = PriceSerializer(data=price_data)
        if price_serializer.is_valid():
            price_serializer.save()
            return JsonResponse("Added Successfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    elif request.method == 'PUT':
        price_data = JSONParser().parse(request)
        price = Price.objects.get(priceId=price_data['priceId'])
        price_serializer = ModelCarSerializer(price, data=price_data)
        if price_serializer.is_valid():
            price_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method == 'DELETE':
        price = Price.objects.get(priceId=id)
        price.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)


@csrf_exempt
def yearApi(request, id=0):
    if request.method == 'GET':
        years = Year.objects.all()
        years_serializer = YearSerializer(years, many=True)
        return JsonResponse(years_serializer.data, safe=False)
    elif request.method == 'POST':
        year_data = JSONParser().parse(request)
        year_serializer = YearSerializer(data=year_data)
        if year_serializer.is_valid():
            year_serializer.save()
            return JsonResponse("Added Successfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    elif request.method == 'PUT':
        year_data = JSONParser().parse(request)
        year = Price.objects.get(yearId=year_data['yearId'])
        year_serializer = ModelCarSerializer(year, data=year_data)
        if year_serializer.is_valid():
            year_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method == 'DELETE':
        year = Year.objects.get(yearId=id)
        year.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)


@csrf_exempt
def fuelApi(request, id=0):
    if request.method == 'GET':
        fuels = Fuel.objects.all()
        fuels_serializer = FuelSerializer(fuels, many=True)
        return JsonResponse(fuels_serializer.data, safe=False)
    elif request.method == 'POST':
        fuel_data = JSONParser().parse(request)
        fuel_serializer = FuelSerializer(data=fuel_data)
        if fuel_serializer.is_valid():
            fuel_serializer.save()
            return JsonResponse("Added Successfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    elif request.method == 'PUT':
        fuel_data = JSONParser().parse(request)
        fuel = Price.objects.get(fuelId=fuel_data['fuelId'])
        fuel_serializer = FuelSerializer(fuel, data=fuel_data)
        if fuel_serializer.is_valid():
            fuel_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method == 'DELETE':
        fuel = Fuel.objects.get(fuelId=id)
        fuel.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)


@csrf_exempt
def carApi(request, id=0):
    if request.method == 'GET':
        cars = Car.objects.all()
        cars_serializer = CarSerializer(cars, many=True)
        return JsonResponse(cars_serializer.data, safe=False)
    elif request.method == 'POST':
        car_data = JSONParser().parse(request)
        car_serializer = CarSerializer(data=car_data)
        if car_serializer.is_valid():
            car_serializer.save()
            return JsonResponse("Added Successfully!!", safe=False)
        return JsonResponse("Failed to Add.", safe=False)
    elif request.method == 'PUT':
        car_data = JSONParser().parse(request)
        car = Car.objects.get(carId=car_data['carId'])
        car_serializer = CarSerializer(car, data=car_data)
        if car_serializer.is_valid():
            car_serializer.save()
            return JsonResponse("Updated Successfully!!", safe=False)
        return JsonResponse("Failed to Update.", safe=False)

    elif request.method == 'DELETE':
        car = Car.objects.get(carId=id)
        car.delete()
        return JsonResponse("Deleted Successfully!!", safe=False)

@csrf_exempt
def SaveFile(request):
    file = request.FILES['uploadedFile']
    file_name = default_storage.save(file.name, file)

    return  JsonResponse(file_name, safe=False)