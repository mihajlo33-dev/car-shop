from django.conf.urls import url
from . import views
from django.conf.urls.static import  static
from django.conf import settings

urlpatterns=[
    url(r'^brand/$', views.brandApi),
    url(r'^brand/([0-9]+)$', views.brandApi),
    url(r'^modelCar/$', views.modelCarApi),
    url(r'^modelCar/([0-9]+)$', views.modelCarApi),
    url(r'^price/$', views.priceApi),
    url(r'^price/([0-9]+)$', views.priceApi),
    url(r'^year/$', views.yearApi),
    url(r'^year/([0-9]+)$', views.yearApi),
    url(r'^fuel/$', views.fuelApi),
    url(r'^fuel/([0-9]+)$', views.fuelApi),
    url(r'^car/$', views.carApi),
    url(r'^car/([0-9]+)$', views.carApi),
    url(r'^SaveFile/$',views.SaveFile)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)