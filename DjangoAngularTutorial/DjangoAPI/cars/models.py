from django.db import models


# Create your models here.


class Brand(models.Model):
    brandId = models.AutoField(primary_key=True)
    brandName = models.CharField(max_length=100)


class ModelCar(models.Model):
    modelId = models.AutoField(primary_key=True)
    modelName = models.CharField(max_length=100)


class Price(models.Model):
    priceId = models.AutoField(primary_key=True)
    price = models.IntegerField()


class Year(models.Model):
    yearId = models.AutoField(primary_key=True)
    year = models.IntegerField()


class Fuel(models.Model):
    fuelId = models.AutoField(primary_key=True)
    fuelName = models.CharField(max_length=100)


class Car(models.Model):
    carId = models.AutoField(primary_key=True)
    carBrand = models.ForeignKey(Brand, on_delete=models.CASCADE, null=True, blank=True)
    carModel = models.ForeignKey(ModelCar, on_delete=models.CASCADE, null=True, blank=True)
    carPrice = models.ForeignKey(Price, on_delete=models.CASCADE, null=True, blank=True)
    carYear = models.ForeignKey(Year, on_delete=models.CASCADE, null=True, blank=True)
    carFuel = models.ForeignKey(Fuel, on_delete=models.CASCADE, null=True, blank=True)
    carDescription = models.CharField(max_length=100, null=True, blank=True)
    PhotoFileName = models.CharField(max_length=100,null=True, blank=True)
    DateOfJoining = models.DateField(null=True, blank=True)