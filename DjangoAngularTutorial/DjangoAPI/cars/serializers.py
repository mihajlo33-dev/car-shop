from rest_framework import serializers
from .models import Brand, ModelCar, Price, Year, Fuel, Car


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('brandId', 'brandName')


class ModelCarSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelCar
        fields = ('modelId', 'modelName')


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = ('priceId', 'price')


class YearSerializer(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = ('yearId', 'year')


class FuelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fuel
        fields = ('fuelId', 'fuelName')


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('carId', 'carBrand', 'carModel', 'carPrice', 'carYear', 'carFuel', 'carDescription','DateOfJoining')

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        rep['carBrand'] = BrandSerializer(instance.carBrand).data
        rep['carModel'] = ModelCarSerializer(instance.carModel).data
        rep['carPrice'] = PriceSerializer(instance.carPrice).data
        rep['carYear'] = YearSerializer(instance.carYear).data
        rep['carFuel'] = FuelSerializer(instance.carFuel).data
        return rep

