# Generated by Django 3.1.6 on 2021-02-03 10:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0002_brand'),
    ]

    operations = [
        migrations.RenameField(
            model_name='brand',
            old_name='BrandId',
            new_name='brandId',
        ),
        migrations.RenameField(
            model_name='brand',
            old_name='BrandName',
            new_name='brandName',
        ),
        migrations.RenameField(
            model_name='car',
            old_name='CarBrand',
            new_name='carBrand',
        ),
        migrations.RenameField(
            model_name='car',
            old_name='CarFuel',
            new_name='carFuel',
        ),
        migrations.RenameField(
            model_name='car',
            old_name='CarId',
            new_name='carId',
        ),
        migrations.RenameField(
            model_name='car',
            old_name='CarModel',
            new_name='carModel',
        ),
        migrations.RenameField(
            model_name='car',
            old_name='CarPrice',
            new_name='carPrice',
        ),
        migrations.RenameField(
            model_name='car',
            old_name='CarYear',
            new_name='carYear',
        ),
    ]
